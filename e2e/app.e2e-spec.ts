import { AdTopusPage } from './app.po';

describe('ad-topus App', function() {
  let page: AdTopusPage;

  beforeEach(() => {
    page = new AdTopusPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
